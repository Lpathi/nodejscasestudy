const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        if (req.headers.authorization) {
            const token = req.headers.authorization.split(' ')[1];
            const decodedData = jwt.verify(token, process.env.server_secret_key);
            req.userData = decodedData;
            next();
        } else {
            throw new Error("Unauthorized request");
        }
    } catch (error) {
        res.status(401).json({
            message: 'You are not Authorized!!'
        });
    }
}