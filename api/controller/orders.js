const mongoose = require('mongoose');
const Order = require('./../models/order');
const Product = require('./../models/product');

exports.get_all_orders = (req, res, next) => {
    Order.find()
    .select('-__v')
    .populate('product', 'productName productPrice')
    .then(docs => {
        if (docs && docs.length > 0) {
            const response = {
                count: docs.length,
                orders: docs
            }
            res.status(200).json(response);
        } else {
            res.status(404).json({
                message: 'No orders founds!!'
            });
        }
    })
    .catch(err => {
        res.status(500).json({
            message: 'Error while fetching orders!!',
            Error: err
        });
    });
};

exports.get_order = (req, res, next) => {
    const orderId = req.params.orderId;
    Order.findById(orderId)
    .select('-__v')
    .populate('product', 'productName productPrice')
    .then(doc => {
        if (doc) {
            res.status(200).json({
                message: 'A specific order has been retrieved!!',
                product: doc
            });
        } else {
            res.status(404).json({
                message: 'No Order found for the specific orderID!!'
            });
        }
    })
    .catch(err => {
        res.status(500).json({
            message: 'Error while fetching the order!!',
            Error: err
        });
    });
}

exports.create_order = (req, res, next) => {
    const productID = req.body.product;

    Product.findById(productID)
    .then(doc => {
        if (doc) {
            const order = new Order({
                _id: new mongoose.Types.ObjectId(),
                orderId: req.body.orderId,
                product: req.body.product,
                quantity: req.body.quantity
            });
            return order.save();
        } else {
            return res.status(404).json({
                message: 'No Product found for the specific productID!!'
            });
        }
    })
    .then(order => {
        if (res.statusCode === 404) {
            return res;
        } else {
            res.status(200).json({
                message: 'A new order has been raised!!',
                createdOrder: order
            });
        }
    })
    .catch(error => {
        res.status(500).json({
            message: 'Error while creating the order!!',
            Error: error
        });
    });
};

exports.delete_order = (req, res, next) => {
    const orderId = req.params.orderId;
    Order.remove({_id: orderId})
    .then(doc => {
        if (doc && doc.deletedCount > 0) {
            res.status(200).json({
                message: 'A specific order has been deleted!!',
                order: doc
            });
        } else {
            res.status(500).json({
                message: 'Error - No order has been found!!'
            });
        }
    })
    .catch(err => {
        res.status(500).json({
            message: 'Error while deleting the order!!',
            Error: err
        });
    });
};