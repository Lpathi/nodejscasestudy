const mongoose = require('mongoose');
const Product = require('./../models/product');

exports.get_all_products = (req, res, next) => {
    Product.find()
    .select('-__v')
    .then(docs => {
        if (docs && docs.length > 0) {
            const response = {
                count: docs.length,
                products: docs
            }
            res.status(200).json(response);
        } else {
            res.status(404).json({
                message: 'No Product founds!!'
            });
        }
    })
    .catch(err => {
        res.status(500).json({
            message: 'Error while fetching the product!!',
            Error: err
        });
    });
}

exports.get_product = (req, res, next) => {
    const productId = req.params.productId;
    Product.findById(productId)
    .select('-__v')
    //.exec()
    .then(doc => {
        console.log(doc)
        if (doc) {
            res.status(200).json({
                message: 'A specific product has been retrieved!!',
                product: doc
            });
        } else {
            res.status(404).json({
                message: 'No Product found for the specific productID!!'
            });
        }
    })
    .catch(err => {
        res.status(500).json({
            message: 'Error while fetching the product!!',
            Error: err
        });
    });
}

exports.post_product = (req, res, next) => {
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        productName: req.body.productName,
        productPrice: req.body.productPrice,
        productImage: req.file.path
    });

    product.save() 
    .then(result => {
        res.status(200).json({
            message: 'A new product has been added!!',
            createdProduct: result
        });
    })
    .catch(err => {
        res.status(500).json({
            message: 'Error while fetching the product!!',
            Error: err
        });
    });
}

exports.delete_Product = (req, res, next) => {
    const productId = req.params.productId;
    Product.remove({_id: productId})
    .then(doc => {
        if (doc && doc.deletedCount > 0) {
            res.status(200).json({
                message: 'A specific product has been deleted!!',
                product: doc
            });
        } else {
            res.status(500).json({
                message: 'Error - No product has been found!!'
            });
        }
    })
    .catch(err => {
        res.status(500).json({
            message: 'Error while deleting the product!!',
            Error: err
        });
    });
}

exports.update_product = (req, res, next) => {
    const productId = req.params.productId;
    const productObj = {};

    for (const prop of req.body) {
        productObj[prop.propName] = prop.value;
    }
    Product.update({_id: productId}, {$set: productObj})
    //.exec()
    .then(doc => {
        if (doc && doc.nModified > 0) {
            res.status(200).json({
                message: 'A specific product has been update!!',
                product: doc
            });
        } else {
            res.status(500).json({
                message: 'Error - No product has been found!!'
            });
        }
    })
    .catch(err => {
        res.status(500).json({
            message: 'Error while updating the product!!',
            Error: err
        });
    });
}